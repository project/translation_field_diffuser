<?php

/**
 * @file
 * Hook implementations and helpers functions for Translation Field Diffuser.
 */

/**
 * Implements hook_field_attach_form().
 */
function translation_field_diffuser_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  module_load_include('inc', 'translation_field_diffuser');
  $entities_translatable = variable_get('translation_field_diffuser_entities_translatable', array());
  if (array_key_exists($entity_type, $entities_translatable)
    && property_exists($entity, 'propagable_fields')) {

    $checkbox_form_api = array(
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#access' => user_access('propagate data'),
      '#attributes' => array(
        'class' => array('translation-field-diffuser-checkbox'),
      ),
    );

    foreach ($entity->propagable_fields as $propagable_field) {
      $number_items   = $propagable_field['number'];
      $field_name     = $propagable_field['field_name'];
      $field_weight   = $form[$field_name]['#weight'];
      $field_language = $form[$field_name]['#language'];
      $checkbox_name  = $field_name . '_' . $field_language . '_';

      for ($i = 0; $i < $number_items; $i++) {
        // Keep items order and make sur none of the item have a weight
        // heavier than the next field.
        $checkbox_weight = $field_weight + ($i * 1 / (100 * $number_items));
        if ($number_items === 1) {
          $checkbox_title = t('Propagate data');
        }
        else {
          $checkbox_title = t('Propagate data (media: @delta)', array('@delta' => $i + 1));
        }

        $form[$checkbox_name . $i . '_propagate'] = $checkbox_form_api + array(
          '#title'  => $checkbox_title,
          '#weight' => $checkbox_weight,
        );
      }
    }

    // Add just once the JS file that moves inside their field and shows the
    // checkboxes.
    $js_path = drupal_get_path('module', 'translation_field_diffuser') . '/js/field-propagation.js';
    if (!isset($form['#attached'])) {
      $form['#attached'] = array();
    }
    if (!isset($form['#attached']['js'])) {
      $form['#attached']['js'] = array();
    }
    if (!isset($form['#attached']['js'][$js_path])) {
      $form['#attached']['js'][$js_path] = array('type' => 'file');
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function translation_field_diffuser_field_widget_form_alter(&$element, &$form_state, $context) {
  $field_translatable = FALSE;
  $field = $context['field'];

  module_load_include('inc', 'translation_field_diffuser');
  $entity_type = $context['instance']['entity_type'];
  switch ($entity_type) {
    case 'node':
      $entity = isset($context['form']['#' . $entity_type]) ? $context['form']['#' . $entity_type] : NULL;
      if (is_object($entity)) {
        $field_translatable = translation_field_diffuser_entity_is_translatable($entity_type, $entity->type, $entity);

        // Field can be propagate only for default content language.
        if (module_exists('entity_translation')
          && property_exists($entity, 'translations')) {
          $translations = $entity->translations;
          $field_translatable &= $translations && $translations->original == $context['langcode'];
        }
        elseif (property_exists($entity, 'tnid')) {
          $field_translatable &= $entity->tnid == $entity->nid;
        }
        else {
          $languages_translatable = variable_get('translation_field_diffuser_languages_translatable', array());
          if (!empty($languages_translatable)) {
            $field_translatable &= in_array($entity->language, $languages_translatable);
          }
        }
      }
      break;

    case 'taxonomy_term':
      $vocabulary = isset($context['form']['#vocabulary']) ? $context['form']['#vocabulary'] : NULL;
      if (is_object($vocabulary)) {
        $field_translatable = translation_field_diffuser_entity_is_translatable('vocabulary', $vocabulary->vocabulary_machine_name, $vocabulary);
        $term = isset($context['form']['#term']) ? $context['form']['#term'] : array();
        if (module_exists('entity_translation')
          && isset($term['translations'])) {
          $translations = $term['translations'];
          $field_translatable &= $translations && $translations->original == $context['langcode'];
        }
        elseif (isset($term['language'])) {
          $field_translatable &= $term['language'] == language_default('language');
        }
      }
      break;

    default:
      break;
  }
  // Allow modules to define if the field is translatable or not.
  drupal_alter('field_is_translatable', $field_translatable, $context);

  // Field is translatable when entity translation is enabled.
  if (module_exists('entity_translation') && entity_translation_enabled($entity_type)) {
    // And field translation is enable.
    $field_translatable &= isset($field['translatable']) && $field['translatable'];
  }

  // Add the ability to propagate field data.
  if ($field_translatable) {
    // When hook_field_widget_form doesn't return bunches of elements.
    if (isset($element['#entity_type'])) {
      translation_field_diffuser_field_element_add_process($element);
    }
    else {
      $number_items = 0;
      foreach ($element as $key => &$elmt) {
        // Numeric key indicate a field delta.
        if (is_numeric($key)) {
          // Media add default item to attach new item to the element form,
          // we don't need to add propagation to this item.
          // @todo This code need to be generic with others field types that
          // are multi value.
          if ($elmt['#type'] != 'media' ||
            ($elmt['#type'] == 'media' && $elmt['#default_value']['fid'])) {
            ++$number_items;
          }
        }
      }

      if ($number_items) {
        translation_field_diffuser_field_element_add_process($element[0], $number_items);
      }
    }

    // @fixme Some multi-value elements doesn't have a theme_wrappers set.
    // Multi value form element inside a fieldset.
    if (($field['cardinality'] > 1 || $field['cardinality'] == FIELD_CARDINALITY_UNLIMITED)
      && isset($element['#theme_wrappers'])
      && is_array($element['#theme_wrappers'])
      && in_array('fieldset', $element['#theme_wrappers'])) {
      $label = $element['#title'];
      $field_propagate_all_items = array(
        '#type' => 'checkbox',
        '#title' => t('Propagate all the data for field @label', array('@label' => $label)),
        '#default_value' => FALSE,
        '#access' => user_access('propagate data'),
        '#attributes' => array('class' => array('translation-field-diffuser-all-data')),
        '#weight' => variable_get('translation_field_diffuser_propagate_widget_weight', 100),
      );

      // This attribute is render just after the element children.
      // @fixme Find a better approach.
      $element['#value'] = drupal_render($field_propagate_all_items);
      $module_path = drupal_get_path('module', 'translation_field_diffuser');
      $js_path = $module_path . '/js/multiple-propagation.js';
      if (!isset($element['#attached'])) {
        $element['#attached'] = array();
      }
      if (!isset($element['#attached']['js'])) {
        $element['#attached']['js'] = array();
      }
      $element['#attached']['js'][$js_path] = array('type' => 'file');
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function translation_field_diffuser_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  if (isset($form['#instance']) && isset($form['#field'])) {
    $field       = $form['#field'];
    $instance    = $form['#instance'];
    $entity_type = $instance['entity_type'];
    $bundle      = $instance['bundle'];

    // Fields are translatable when content translation is enabled.
    $field_translatable = TRUE;
    if (module_exists('entity_translation')) {
      $enabled_types = array_filter(variable_get('entity_translation_entity_types', array()));

      if (isset($enabled_types[$entity_type])) {
        $field_translatable = isset($field['translatable']) && $field['translatable'];
      }
    }

    module_load_include('inc', 'translation_field_diffuser');
    if (translation_field_diffuser_entity_is_translatable($entity_type, $bundle)
      && $field_translatable) {
      $name = 'translation_field_diffuser_' . $entity_type . '_' . $bundle;
      $name .= '_' . $instance['field_name'];
      $desc = t('Propagate field data from default entity language to others languages');

      $form['propagate'] = array(
        '#type' => 'fieldset',
        '#title' => t('Translation Field Diffuser Settings'),
        '#description' => '<p>' . $desc . '</p>',
        '#tree' => TRUE,
        'field' => array(
          '#type' => 'checkbox',
          '#title' => t('Can this field be propagate ?'),
          '#default_value' => variable_get($name . '_value', FALSE),
        ),
      );

      $form['#submit'][] = 'translation_field_diffuser_form_field_ui_submit';
    }
  }
}

/**
 * Save into variable propagation information for some field.
 *
 * @param array $form
 *   The form structure.
 * @param array $form_state
 *   The current state of the form.
 */
function translation_field_diffuser_form_field_ui_submit(array &$form, array &$form_state) {
  $instance = $form['#instance'];
  $propagable_field = $form_state['values']['propagate']['field'];

  $name = 'translation_field_diffuser_' . $instance['entity_type'];
  $state = variable_get($name . '_value', FALSE);
  variable_set($name . '_value', $state || $propagable_field);

  $name .= '_' . $instance['bundle'];
  $state = variable_get($name . '_value', FALSE);
  variable_set($name . '_value', $state || $propagable_field);

  $name .= '_' . $instance['field_name'];
  variable_set($name . '_value', (bool) $propagable_field);
}

/**
 * Implements hook_form_alter().
 */
function translation_field_diffuser_form_alter(&$form, &$form_state, $form_id) {
  // Default to empty array to avoid getting heavy list for a default value.
  $entities_translatable = variable_get('translation_field_diffuser_entities_translatable', array());

  $entity_form_translatable = FALSE;
  foreach ($entities_translatable as $entity_name => $bundles) {
    // @fixme taxonomy_term form id look like 'taxonomy_form_term'. Idea to match properly ?
    if (strpos($form_id, $entity_name . '_form') !== FALSE) {
      $entity_form_translatable = TRUE;
    }
  }

  if ($entity_form_translatable) {
    $form['#submit'][] = 'translation_field_diffuser_entity_submit';
  }
}

/**
 * Store into the entity the information about element form propagation.
 *
 * When entity is translatable with module i18n or Content Translation
 * invoke functions to propagate the field data of the entity.
 *
 * @param array $form
 *   The form structure.
 * @param array $form_state
 *   The current state of the form.
 */
function translation_field_diffuser_entity_submit(array &$form, array &$form_state) {
  $entity = $form['#entity'];
  if (property_exists($entity, 'propagable_fields')) {
    module_load_include('inc', 'translation_field_diffuser');
    $entity_type = $form['#entity_type'];
    $bundle      = isset($form['#bundle']) ? $form['#bundle'] : $entity->type;

    $can_propagate = FALSE;
    switch ($entity_type) {
      case 'node':
        if (translation_field_diffuser_entity_is_translatable($entity_type, $bundle)) {
          $can_propagate = function_exists('translation_supported_type') ? translation_supported_type($bundle) : FALSE;
          $can_propagate |= function_exists('i18n_node_type_enabled') ? i18n_node_type_enabled($bundle) : FALSE;
        }
        break;

      case 'taxonomy_term':
        $vacobulary_name = $entity->vocabulary_machine_name;
        if (module_exists('i18n_taxonomy')
          && translation_field_diffuser_entity_is_translatable($entity_type, $vacobulary_name, $entity)) {
          $vocabulary = taxonomy_vocabulary_machine_name_load($vacobulary_name);
          $can_propagate = i18n_taxonomy_vocabulary_mode($vocabulary) == I18N_MODE_TRANSLATE;
        }
        break;

      default:
        break;
    }

    // Restrain field's data propagation to modules Translation and i18n.
    if ($can_propagate) {
      $values = $form_state['values'];
      $entity->data_propagation = array();

      foreach ($entity->propagable_fields as $propagable_field) {
        $number_items = $propagable_field['number'];
        $field_name = $propagable_field['field_name'];
        // @todo need reset ??
        $field_language = key($entity->{$field_name});
        $checkbox_field_name = $field_name . '_' . $field_language;

        for ($i = 0; $i < $number_items; $i++) {
          $name = $checkbox_field_name . '_' . $i . '_propagate';
          if (isset($values[$name]) && $values[$name]) {
            $entity->data_propagation[] = $field_name . '-' . $i;
          }
        }
      }

      module_invoke_all('data_propagation', $entity, $entity_type);
    }
  }
}

/**
 * Implements hook_data_propagation().
 *
 * Propagate selected entity field's data.
 */
function translation_field_diffuser_data_propagation($entity, $type) {
  if ($type == 'node') {
    if (property_exists($entity, 'data_propagation')
      && !empty($entity->data_propagation)) {
      // Update nodes with non default language.
      $query = db_select('node', 'n')
        ->condition('n.nid', $entity->nid, '!=')
        ->condition('n.tnid', $entity->nid);
      $query->addField('n', 'nid');
      $nids = $query->execute()->fetchCol();

      if (is_array($nids)) {
        $data_propagation_reorder = array();
        foreach ($entity->data_propagation as $value) {
          list($field_name, $delta) = explode('-', $value);
          $data_propagation_reorder[$field_name]['delta'][$delta] = $delta;
        }

        foreach ($nids as $nid) {
          if ($node_to_update = node_load($nid)) {
            foreach ($entity->data_propagation as $value) {
              list ($field_name, $delta) = explode('-', $value);
              $number_delta_to_propagate = count($data_propagation_reorder[$field_name]['delta']);
              $lang = $entity->language;
              $lang_update = $node_to_update->language;

              if (array_key_exists(LANGUAGE_NONE, $node_to_update->{$field_name})) {
                $lang = $lang_update = LANGUAGE_NONE;
              }

              // All items in the element are in propagation.
              if ($number_delta_to_propagate === count($entity->{$field_name}[$lang])) {
                $node_to_update->{$field_name}[$lang_update] = $entity->{$field_name}[$lang];
              }
              elseif (isset($entity->{$field_name}[$lang][$delta])) {
                $node_to_update->{$field_name}[$lang_update][$delta] = $entity->{$field_name}[$lang][$delta];
              }
            }

            node_save($node_to_update);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_permission().
 */
function translation_field_diffuser_permission() {
  return array(
    'propagate data' => array(
      'title' => t('User can propagate field data'),
    ),
  );
}

/**
 * Add checkbox into field widget to allow user to propagate field data.
 *
 * @param array $element
 *   The field widget form element.
 * @param array $form_state
 *   The current state of the form.
 * @param array $form
 *   The form structure of the field widget.
 *
 * @return array
 *   The field widget form element with additional checkbox
 *   to propagate the field's data.
 */
function translation_field_diffuser_widget_process(array $element, array &$form_state, array $form) {
  $label = isset($element['#title']) ? $element['#title'] : '';
  if (empty($label)) {
    $instance = field_widget_instance($element, $form_state);
    $label    = $instance['label'];
  }

  $field_id = $element['#field_name'] . '_propagate_' . $element['#weight'];
  $field_propagate_checkbox = array(
    '#type' => 'checkbox',
    '#title' => t('Propagate data for field @label', array('@label' => $label)),
    '#default_value' => FALSE,
    '#access' => user_access('propagate data'),
    '#attributes' => array(
      'id' => $field_id,
      'class' => array('translation-field-diffuser-widget'),
    ),
    '#weight' => variable_get('translation_field_diffuser_propagate_widget_weight', 100),
  );

  // Some themes doesn't render children of an element.
  // We need to use the suffix of the field.
  $propagate_checkbox = drupal_render($field_propagate_checkbox);
  if (!isset($element['#suffix'])) {
    $element['#suffix'] = $propagate_checkbox;
  }
  else {
    $element['#suffix'] .= $propagate_checkbox;
  }

  // @todo Find a way to attached only once the CSS (in case of multiple
  // propagable fields)
  $module_path = drupal_get_path('module', 'translation_field_diffuser');
  $element['#attached']['css'][] = $module_path . '/css/field-widget.css';

  return $element;
}
